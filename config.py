class Config():
    MUSIC_LIBRARY_DIR = "/home/pappa/Music"
    COVER_DIR = "/home/pappa/Pictures/mpdcovers"
    JOYSTICK = False
    DEBUG = True
    DEBUG_WIDTH = 480
    DEBUG_HEIGHT = 320
    MPD = False
    STREAM = True
    #STREAM_URL = "http://streams.radiobob.de/bob-wacken/mp3-192/"
    STREAMS = {
        "Wacken": "http://streams.radiobob.de/bob-wacken/mp3-192/",
        "Death Metal": "http://streams.radiobob.de/deathmetal/mp3-192/streams.radiobob.de/",
        "Metalcore": "http://streams.radiobob.de/metalcore/mp3-192/streams.radiobob.de/",
        "Symphonic Metal": "http://streams.radiobob.de/symphmetal/mp3-192/streams.radiobob.de/",
        "Rockparty": "http://streams.radiobob.de/rockparty/mp3-192/streams.radiobob.de/",
        "Prog Rock": "http://streams.radiobob.de/progrock/mp3-192/streams.radiobob.de/",
        "Härte Seite": "http://streams.radiobob.de/bob-hartesaite/mp3-128/streams.radiobob.de/",
        "Mittelälter": "http://streams.radiobob.de/mittelalter/mp3-192/streams.radiobob.de/",
        "Power Metal": "http://streams.radiobob.de/powermetal/mp3-192/streams.radiobob.de/",
        "Metal": "http://streams.radiobob.de/bob-metal/mp3-192/streams.radiobob.de/",
        "Women of Rock": "http://streams.radiobob.de/womenofrock/mp3-192/streams.radiobob.de/",
        "2000 Rock": "http://streams.radiobob.de/2000er/mp3-192/streams.radiobob.de/",
        "90 Rock": "http://streams.radiobob.de/bob-90srock/mp3-192/streams.radiobob.de/",
    }
    BUTTONS = True
