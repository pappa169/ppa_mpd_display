
import logging

from mpd import MPDClient
from PyQt5 import QtCore
from mpd import CommandError
import typing
from typing import List

logger = logging.getLogger(__name__)


class MpdControl(QtCore.QObject):
    def __init__(self):
        QtCore.QObject.__init__(self)
        self._client = self._connect()
        self._client.timeout = 1
        self.__exit = False

    def disconnect(self):
        self._client.close()
        self._client.disconnect()

    @staticmethod
    def _connect():
        client = MPDClient()           # create _client object
        client.connect("localhost", 6600)  # connect to localhost:6600
        return client

    @staticmethod
    def _disconnect(client: MPDClient):
        client.close()
        client.disconnect()

    def songInfo(self) -> dict:
        song_info = {}
        info = self._client.currentsong()
        if 'artist' not in info.keys():
            song_info['artist'] = 'artist'
        else:
            song_info['artist'] = info['artist']
        if 'title' not in info.keys():
            song_info['title'] = 'title'
        else:
            song_info['title'] = info['title']
        if 'album' not in info.keys():
            song_info['album'] = 'album'
        else:
            song_info['album'] = info['album']
        if 'file' in info.keys():
            song_info['file'] = info['file']
        song_info['state'] = self._client.status()['state']
        return song_info

    @staticmethod
    def next():
        client = MpdControl._connect()
        client.next()
        MpdControl._disconnect(client)
        
    @staticmethod
    def prev():
        client = MpdControl._connect()
        client.previous()
        MpdControl._disconnect(client)

    @staticmethod
    def stopSong():
        client = MpdControl._connect()
        client.stop()
        MpdControl._disconnect(client)

    @staticmethod
    def isRandom():
        client = MpdControl._connect()
        random = client.status()['random']
        MpdControl._disconnect(client)
        return random

    @staticmethod
    def setRandom(random):
        client = MpdControl._connect()
        client.random(int(random))
        MpdControl._disconnect(client)

    def playSong(self, song):
        self._client.play(song)

    def play(self):
        self._client.play()

    def findArtists(self):
        return [artistDict.get('artist') for artistDict in self._client.list('artist')]

    def findAlbums(self, artist: str) -> List[str]:
        return [albumDict.get('album') for albumDict in self._client.list('album', artist)]

    def findSongs(self, album):
        return [songDict.get('title') for songDict in self._client.list('title', "Album", album)]

    def findSongInPlaylist(self, artist, album, song):
        song = self._client.playlistfind('artist', artist, 'album', album, 'title', song)
        return song

    def isSongStarred(self, uri):
        try:
            return self._client.sticker_get("song", uri, "star") == "1"
        except CommandError:
            return False

    def starSong(self, uri, star):
        self._client.sticker_set("song", uri, "star", "1" if star else "0")

    def getStarredSongs(self):
        starredSongs: List[str] = []
        for file in self._client.lsinfo("/"):
            if 'directory' in file.keys():
                starredSongs += [starred['file'] for starred in self._client.sticker_find("song", file['directory'], "star")]
        return starredSongs

    def playFile(self, file):
        song = self._client.playlistfind('file', file)
        exists = len(song) > 0
        if exists:
            self.playSong(song[0]['pos'])
        return exists

    reload_sig = QtCore.pyqtSignal()

    def idle(self):
        while not self.__exit:
            #print 'Idleloop'
            self._client.idle('player')
            self.reload_sig.emit()
        self._client.close()
        self._client.disconnect()

    def stop(self):
        self.__exit = True

    @staticmethod
    def playlistLength():
        client = MpdControl._connect()
        playlistLen = client.status()['playlistlength']
        MpdControl._disconnect(client)
        return playlistLen

    @staticmethod
    def addAllToPlaylist():
        client = MpdControl._connect()

        for musicItem in client.listfiles():
            if 'directory' in musicItem:
                if not musicItem['directory'].startswith('.'):
                    try:
                        client.add(musicItem['directory'])
                    except CommandError as err:
                        logger.error(err)
                        pass
            elif 'file' in musicItem:
                print(musicItem['file'])
                try:
                    client.add(musicItem['file'])
                except CommandError:
                    pass
        MpdControl._disconnect(client)
