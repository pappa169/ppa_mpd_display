from PyQt5 import QtCore,QtWidgets,QtGui
from my_mpd import MpdControl
from enum import Enum
from typing import List


class MENU_ROWS(Enum):
    ARTIST = 0
    ALBUM = 1
    SONG = 2
    RANDOM = 3
    STAR = 4
    FINISH = 5


class QuickMenu:
    def __init__(self, parent_widget, cleanup):
        self.__cleanup = cleanup
        #timer
        self._menuTimer = QtCore.QTimer()
        self._menuTimer.setInterval(5000)
        self._menuTimer.timeout.connect(self.hide)
        #table
        self.table =  QtWidgets.QTableWidget(MENU_ROWS.FINISH.value, 2, parent_widget)
        self.table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.table.resize(656,300)
        self.table.verticalHeader().hide()
        self.table.horizontalHeader().hide()
        header = self.table.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        font = QtGui.QFont("Sans", 18, QtGui.QFont.Normal)
        self.table.setFont(font)
        rows = ['Artist', 'Album', 'Song', 'Random', 'Star', 'Apply']
        self.table.setRowCount(len(rows))
        for row in range(self.table.rowCount()):
            self.table.setItem(row, 0, QtWidgets.QTableWidgetItem(rows[row]))
        self._mpd_artists: List[str] = []
        self._currentRow: int = 0
        self._currentArtist: int = 0
        self._mpd_albums: List[str] = []
        self._currentAlbum: int = 0
        self._mpd_songs: List[str] = []
        self._currentTitle: int = 0
        self._shown: bool = False
        self._changed: bool = False

    def rowDown(self):
        self._menuTimer.start()
        if self._currentRow < self.table.rowCount():
            self._currentRow += 1
            self.table.selectRow(self._currentRow)
        self._changed = self._currentRow == MENU_ROWS.FINISH.value

    def rowUp(self):
        self._menuTimer.start()
        if self._currentRow > 0:
            self._currentRow -= 1
            self.table.selectRow(self._currentRow)
        self._changed = self._currentRow == MENU_ROWS.FINISH.value

    def changeNext(self):
        self._menuTimer.start()
        if self._currentRow == MENU_ROWS.ARTIST.value:
            self._currentArtist += 1
            self._currentArtist = self._currentArtist % len(self._mpd_artists)
            self._currentAlbum = 0
            self._currentTitle = 0
        elif self._currentRow == MENU_ROWS.ALBUM.value:
            self._currentAlbum += 1
            self._currentAlbum = self._currentAlbum % len(self._mpd_albums)
            self._currentTitle = 0
        elif self._currentRow == MENU_ROWS.SONG.value:
            self._currentTitle += 1
            self._currentTitle = self._currentTitle % len(self._mpd_songs)
        elif self._currentRow == MENU_ROWS.STAR.value:
            self._toggleStar()
        elif self._currentRow == MENU_ROWS.RANDOM.value:
            MpdControl.setRandom(not (self.table.item(3, 1).text() == 'Yes'))

        self._updateTable()

    def changePrev(self):
        self._menuTimer.start()
        if self._currentRow == MENU_ROWS.ARTIST.value:
            self._currentArtist -= 1
            self._currentArtist = abs(self._currentArtist % len(self._mpd_artists))
            self._currentAlbum = 0
            self._currentTitle = 0
        elif self._currentRow == MENU_ROWS.ALBUM.value:
            self._currentAlbum -= 1
            self._currentAlbum = abs(self._currentAlbum % len(self._mpd_albums))
            self._currentTitle = 0
        elif self._currentRow == MENU_ROWS.SONG.value:
            self._currentTitle -= 1
            self._currentTitle = abs(self._currentTitle % len(self._mpd_songs))
        elif self._currentRow == MENU_ROWS.STAR.value:
            self._toggleStar()
        elif self._currentRow == MENU_ROWS.RANDOM.value:
            MpdControl.setRandom(not (self.table.item(3, 1).text() == 'Yes'))

        self._updateTable()

    def _toggleStar(self):
        mpd = MpdControl()
        song = mpd.findSongInPlaylist(self._mpd_artists[self._currentArtist],
                                      self._mpd_albums[self._currentAlbum],
                                      self._mpd_songs[self._currentTitle])
        if len(song) > 0:
            mpd.starSong(song[0]['file'], not (mpd.isSongStarred(song[0]['file'])))

    def _updateTable(self):
        mpdQuery = MpdControl()
        if len(self._mpd_artists) > 0:
            self._mpd_albums = mpdQuery.findAlbums(self._mpd_artists[self._currentArtist])
        if len(self._mpd_albums) > 0:
            self._mpd_songs = mpdQuery.findSongs(self._mpd_albums[self._currentAlbum])
        artist = '' if len(self._mpd_artists) == 0 else self._mpd_artists[self._currentArtist]
        album = '' if len(self._mpd_albums) == 0 else self._mpd_albums[self._currentAlbum]
        title = '' if len(self._mpd_songs) == 0 else self._mpd_songs[self._currentTitle]
        self.table.setItem(0, 1, QtWidgets.QTableWidgetItem(artist))
        self.table.setItem(1, 1, QtWidgets.QTableWidgetItem(album))
        self.table.setItem(2, 1, QtWidgets.QTableWidgetItem(title))
        self.table.setItem(3, 1, QtWidgets.QTableWidgetItem('Yes' if MpdControl.isRandom() == '1' else 'No'))
        #print(self._currentArtist)
        song = mpdQuery.findSongInPlaylist(self._mpd_artists[self._currentArtist], self._mpd_albums[self._currentAlbum],
                                           self._mpd_songs[self._currentTitle])
        if len(song) > 0:
            self.table.setItem(4, 1, QtWidgets.QTableWidgetItem('Yes' if mpdQuery.isSongStarred(song[0]['file']) == True else 'No'))
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()
        mpdQuery.disconnect()

    def show(self):
        self._shown = True
        mpdQuery = MpdControl()
        if len(self._mpd_artists) == 0:
            #print("searching artists")
            self._mpd_artists = mpdQuery.findArtists()
        #find current song
        songInfo = mpdQuery.songInfo()
        if songInfo['artist'] in self._mpd_artists:
            self._currentArtist = self._mpd_artists.index(songInfo['artist'])
        if len(self._mpd_artists) > 0:
            self._mpd_albums = mpdQuery.findAlbums(self._mpd_artists[self._currentArtist])
        if songInfo['album'] in self._mpd_albums:
            self._currentAlbum = self._mpd_albums.index(songInfo['album'])
        if len(self._mpd_albums) > 0:
            self._mpd_songs = mpdQuery.findSongs(self._mpd_albums[self._currentAlbum])
        if songInfo['title'] in self._mpd_songs:
            self._currentTitle = self._mpd_songs.index(songInfo['title'])
        self._updateTable()
        mpdQuery.disconnect()
        self._changed = False
        #show table and start timer
        self.table.show()
        self.table.selectRow(self._currentRow)
        self._menuTimer.start()

    def hide(self):
        mpd = MpdControl()
        song = mpd.findSongInPlaylist(self._mpd_artists[self._currentArtist], self._mpd_albums[self._currentAlbum],
                                      self._mpd_songs[self._currentTitle])
        if self._changed and len(song) > 0:
            mpd.playSong(song[0]['pos'])
        mpd.disconnect()
        self.table.hide()
        self._menuTimer.stop()
        self._shown = False
        self.__cleanup()
    
    def isShown(self):
        return self._shown
    