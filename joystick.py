import Adafruit_ADS1x15
import time

from enum import IntEnum
from PyQt5 import QtCore


class DIRECTION(IntEnum):
    STILL = 0
    FORWARD = 1
    BACKWARD = 2
    LEFT = 3
    RIGHT = 4
    LONG_FORWARD = 5
    LONG_BACKWARD = 6


class Joystick(QtCore.QObject):
    def __init__(self):
        QtCore.QObject.__init__(self)
        self.__directionX = DIRECTION.STILL
        self.__directionY = DIRECTION.STILL
        self.__oldDirectionX = DIRECTION.STILL
        self.__oldDirectionY = DIRECTION.STILL
        self.__exit = False
        self.__longPress = False
    
    joystick_sig = QtCore.pyqtSignal(int)
    
    # longWait how many cycle to wait to get a long press
    def loop(self, longWait):
        adc = Adafruit_ADS1x15.ADS1115()
        longPress = False
        longPressCount = longWait
        while not self.__exit:
            values = [0]*2
            for i in range(2):
                # Read the specified ADC channel using the previously set gain value.
                values[i] =  adc.read_adc(i, gain=1)
            #print(values)
            self.__directionX = DIRECTION.STILL
            self.__directionY = DIRECTION.STILL
            
            if(values[0] <= 15000):
                self.__directionX = DIRECTION.BACKWARD;
            elif (values[0] > 30000 and values[0] <= 32767):
                self.__directionX = DIRECTION.FORWARD;
            else:
                self.__directionX = DIRECTION.STILL;
            if(values[1] <= 15000):
                self.__directionY = DIRECTION.LEFT
            elif (values[1] > 30000 and values[1] <= 32767):
                self.__directionY = DIRECTION.RIGHT
            else:
                self.__directionY = DIRECTION.STILL;
            
            changedDirectionX = False
            if (self.__directionX != self.__oldDirectionX):
                changedDirectionX = True
            
            changedDirectionY = False
            if (self.__directionY != self.__oldDirectionY):
                changedDirectionY = True
                        
            if longPress == False and changedDirectionX ^ changedDirectionY:
                #print('Started long')
                longPress = True
                continue
            
            if longPress:
                if self.__directionX == DIRECTION.STILL and self.__directionY == DIRECTION.STILL: #end press, normal
                    if longPressCount != longWait:
                        #print('Pressed short:', self.__getOldDirection())
                        if self.__getOldDirection() != None:
                            self.joystick_sig.emit(self.__getOldDirection())
                        longPressCount = longWait
                        longPress = False
                else:    
                    longPressCount = longPressCount - 1
            
                if longPressCount == 0:
                    #print('Pressed long:', self.__getLongDirection())
                    if self.__getLongDirection() != None:
                        self.joystick_sig.emit(self.__getLongDirection())
                    longPressCount = longWait
                    longPress = False
            
            self.__oldDirectionX = self.__directionX
            self.__oldDirectionY = self.__directionY
            
            time.sleep(0.1)
        #print('Done Joystick loop')
            
    def __getOldDirection(self):
        if self.__oldDirectionX != DIRECTION.STILL :
            return self.__oldDirectionX
        elif self.__oldDirectionY != DIRECTION.STILL :
            return self.__oldDirectionY
        return None
        
    def __getLongDirection(self):
        if self.__directionX != DIRECTION.STILL :
            if self.__directionX == DIRECTION.FORWARD:
                return DIRECTION.LONG_FORWARD;
            elif self.__directionX == DIRECTION.BACKWARD:
                return DIRECTION.LONG_BACKWARD
        return None

    def stop(self):
        self.__exit = True
         
            
        

