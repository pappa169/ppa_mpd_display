import RPi.GPIO as GPIO
from typing import Callable


class Raspi:

    def __init__(self):
        pass

    @staticmethod
    def setupNextButton(callback: Callable):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(4, GPIO.FALLING, callback=callback, bouncetime=2000)
