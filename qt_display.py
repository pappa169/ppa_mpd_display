#!/usr/bin/python
import sys
import os
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QPushButton, QSlider, QComboBox

import volume
from cover import CoverManager
from my_mpd import MpdControl
from config import Config
from quick_menu import QuickMenu
from star_menu import StarMenu
from joystick import Joystick, DIRECTION
from stream_player import StreamPlayer
from typing import Dict, Tuple, Optional
import logging
if not Config.DEBUG:
    from raspi import Raspi

SongInfoKeys = Tuple['artist', 'album', 'title', 'state']
SongInfo = Dict[str, str]

logger = logging.getLogger(__name__)


class MyWidget(QtWidgets.QMainWindow):
    """
    Custom widget used for the main widget of the MyApp
    Handles custom key controls(from joystick or keyboard)
    """
    # signals
    menu_down_sig = QtCore.pyqtSignal()
    menu_up_sig = QtCore.pyqtSignal()
    menu_left_sig = QtCore.pyqtSignal()
    menu_right_sig = QtCore.pyqtSignal()

    def __init__(self):
        super(MyWidget, self).__init__()
        self._count = 0 # key auto-repeat count in pressed state
        self._timer = QtCore.QTimer()
        self._timer.setInterval(2000)
        self._timer.timeout.connect(self._timeout)

    def _timeout(self):
        self._timer.stop()

    def keyPressEvent(self, e):
        super(MyWidget, self).keyPressEvent(e)       
        if e.isAutoRepeat(): 
            self._count += 1
            if self._count == 3:
                myApp.reboot_sig.emit()
        else:
            self._count = 0
        e.accept()

        if e.key() == QtCore.Qt.Key_Escape:
            myApp.shutdown()
            self.close()
        elif e.key() == QtCore.Qt.Key_M:
            if Config.MPD:
                myApp.showMenu()
        elif e.key() == QtCore.Qt.Key_S:
            if Config.MPD:
                myApp.showStarMenu()
        elif e.key() == QtCore.Qt.Key_Down:
            if Config.MPD:
                self.menu_down_sig.emit()
        elif e.key() == QtCore.Qt.Key_Up:
            if Config.MPD:
                self.menu_up_sig.emit()
        elif e.key() == QtCore.Qt.Key_Left:
            if Config.MPD:
                self.menu_left_sig.emit()
        elif e.key() == QtCore.Qt.Key_Right:
            if Config.MPD:
                self.menu_right_sig.emit()
        elif self._count == 0:
            # next is possible every two seconds
            if Config.MPD:
                if not self._timer.isActive():
                    MpdControl.next()
                    self._timer.start()
                else:
                    print ('MPD command running')

    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        myApp.shutdown()


class IdleThread(QtCore.QThread):
    """
    Idle thread for mpd
    Runs the mpdInfo.idle() in a separate thread
    The mpdInfo emits a Qt reload signal to update the song information on song change
    """
    def __init__(self):
        QtCore.QThread.__init__(self)
        self.finished.connect(self.onFinish)
        self.mpdInfo = MpdControl()

#    def __del__(self):
#        self.wait()

    def stop(self):
        self.mpdInfo.stop()
        MpdControl.stopSong()

    def run(self):
        self.mpdInfo.idle()

    def onFinish(self):
        MpdControl.stopSong()
        
        
class JoystickThread(QtCore.QThread):
    def __init__(self):
        QtCore.QThread.__init__(self)
        self.joystick = Joystick()
#        self.finished.connect(self.onFinish)
        
    def __del__(self):
        self.wait()

    def stop(self):
        self.joystick.stop()

    def run(self):
        self.joystick.loop(10)

    def onFinish(self):
        pass


class MyApp(QtCore.QObject):
    
    def __init__(self):
        self._shutdown: bool = False
        self.new_text = []
        self.coverManager = CoverManager()
        QtCore.QObject.__init__(self)
        self.app = QtWidgets.QApplication(sys.argv)
        screen_resolution = self.app.desktop().screenGeometry()
        if Config.DEBUG:
            self.width = Config.DEBUG_WIDTH
            self.height = Config.DEBUG_HEIGHT
        else:
            self.width = screen_resolution.width()
            self.height = screen_resolution.height()
        #   print "width:", width, height

        # construct widget
        self._widget = MyWidget()
        self._background = QtWidgets.QLabel(self._widget)
        if not Config.DEBUG:
            self._background.setGeometry(screen_resolution)
        else:
            self._widget.resize(self.width, self.height)
            self._background.resize(self.width, self.height)
        self._reloadImage()
        self.labelArtist = QtWidgets.QLabel(self._widget)
        self.labelTitle = QtWidgets.QLabel(self._widget)
        self.labelAlbum = QtWidgets.QLabel(self._widget)
        if not Config.DEBUG:
            self._widget.showFullScreen()
        # add buttons
        if Config.BUTTONS:
            self.haltButton = QPushButton('Halt', self._widget)
            self.haltButton.resize(100, 50)
            self.haltButton.move(self.width - 110, 220)
            self.haltButton.pressed.connect(lambda: logger.error(os.system('sudo halt')))
            self.volumeSlider = QSlider(Qt.Horizontal, self._widget)
            self.volumeSlider.resize(self.width, 60)
            self.volumeSlider.setTickPosition(QSlider.TicksRight)
            self.volumeSlider.setTickInterval(5)
            self.volumeSlider.setMinimum(0)
            self.volumeSlider.setMaximum(100)
            self.volumeSlider.move(0, self.height - 60)
            self.volumeSlider.setValue(volume.GetVolume())
            self.volumeSlider.valueChanged.connect(lambda: volume.SetVolume(self.volumeSlider.value()))
            # combo box
            self._stream_cb = QComboBox(self._widget)
            self._stream_cb.move(0, 220)
            for radio in Config.STREAMS.keys():
                self._stream_cb.addItem(radio)
            self._stream_cb.activated.connect(self._changeStream)

            # self.QtWidgets.QLabel(self.widget)
        self._widget.show()

        self._streamPlayer: Optional[StreamPlayer] = None
        self._startStreamPlayer(Config.STREAMS.get(self._stream_cb.currentText(), None))

        self.reload()

        if not Config.DEBUG:
            Raspi.setupNextButton(self.buttonPressed)
        self.menu = None
        self.starMenu = None
        self._idleThread: IdleThread = None
        self._joystickThread: JoystickThread = None

    # signals
    reboot_sig = QtCore.pyqtSignal()

    @property
    def streamPlayer(self):
        return self._streamPlayer

    def _startStreamPlayer(self, defaultStream: str):
        assert self._streamPlayer is None
        if Config.STREAM and defaultStream is not None and self._streamPlayer is None:
            self._streamPlayer = StreamPlayer(defaultStream, self.reload)
            self._streamPlayer.run()

    def shutdown(self):
        self._shutdown = True
        self._streamPlayer.shutdown()
        # stop thread
        if Config.MPD:
            self.idleThread.stop()
            self.idleThread.wait()
        if Config.STREAM:
            self.streamPlayer.stop()
        if Config.JOYSTICK:
            self.joystickThread.stop()
            self.joystickThread.wait()

    def _changeStream(self):
        self._streamPlayer.shutdown()
        self._streamPlayer.stop()
        newStream = Config.STREAMS.get(self._stream_cb.currentText(), None)
        if newStream is not None:
            self._streamPlayer.streamUrl = newStream
        self._streamPlayer.run()

    @staticmethod
    def buttonPressed(self, channel):
        MpdControl.next()
        logger.debug(f'button pressed on {channel}')

    def _inMenu(self):
        if self.menu is not None and self.menu.isShown():
            return True
        if self.starMenu is not None and self.starMenu.isShown():
            return True
        return False
    
    def nextTrack(self):
        if not self._inMenu():
            MpdControl.next()
        
    def prevTrack(self):
        if not self._inMenu():
            MpdControl.prev()

    @property
    def idleThread(self):
        return self._idleThread

    @property
    def joystickThread(self):
        return self._joystickThread

    def startIdleThread(self):
        self._idleThread = IdleThread()
        self._idleThread.mpdInfo.reload_sig.connect(self.reload)
        self._idleThread.start()
        # start joystick thread
        if Config.JOYSTICK:
            self._joystickThread = JoystickThread()
            self._joystickThread.start()
            self._joystickThread.joystick.joystick_sig.connect(self.joystickEvent)
    
    def joystickEvent(self, direction: DIRECTION):
        if Config.MPD:
            if direction == DIRECTION.FORWARD:
                self._widget.menu_up_sig.emit()
                self.nextTrack()
            elif direction == DIRECTION.BACKWARD:
                self._widget.menu_down_sig.emit()
                self.prevTrack()
            elif direction == DIRECTION.LEFT:
                self._widget.menu_left_sig.emit()
                self.prevTrack()
            elif direction == DIRECTION.RIGHT:
                self._widget.menu_right_sig.emit()
                self.nextTrack()
            elif direction == DIRECTION.LONG_FORWARD:
                self.showMenu()
            elif direction == DIRECTION.LONG_BACKWARD:
                self.showStarMenu()

    def run(self):
        sys.exit(self.app.exec_())

    def _setNewText(self, songInfo: SongInfo):
        self.new_text = songInfo

    def _addLabelText(self, label, text):
        # label.setStyleSheet("QLabel { color : rgb(255,255,153);}")
        label.setStyleSheet("QLabel { color : rgb(204, 0, 0);}")
        fontsize = 51
        max_width = 0
        height = 0
        if text:
            while max_width == 0 or max_width > self.width:
                fontsize -= 1
                font = QtGui.QFont("AGENTORANGE", fontsize, QtGui.QFont.Bold)
                metrics = QtGui.QFontMetrics(font)
                max_width = metrics.width(text)
                height = metrics.height()
            label.setText(text)
            label.setFont(font)
            label.adjustSize()
        return max_width, height
    
    def _centerLabels(self, w1, h1, w2, h2, w3, h3):
        self.labelArtist.move(int(round((self.width - w1)/2)), int(round((self.height - h1 - h2 - h3)/2)))
        self.labelTitle.move(int(round((self.width - w2)/2)), int(round((self.height - h2 - h3)/2 + 20)))
        self.labelAlbum.move(int(round((self.width - w3)/2)), int(round((self.height - h3)/2 + 40)))

    def _changeText(self):
        text = ' '
        if not self.coverManager.hasCover():
            text = self.new_text['artist']
        w1, h1 = self._addLabelText(self.labelArtist, text)
        w2, h2 = self._addLabelText(self.labelTitle, self.new_text['title'])
        if not self.coverManager.hasCover():
            text = self.new_text['album']
        w3, h3 = self._addLabelText(self.labelAlbum, text)
        self.labelArtist.move(0, 0)
        self.labelTitle.move(0, 0)
        self.labelAlbum.move(0, 0)
        self._centerLabels(w1, h1, w2, h2, w3, h3)

    def _reloadImage(self):
        cover = self.coverManager.getCover(self.new_text)
        if cover is not None:
            pixmap = QtGui.QPixmap(cover)
            self._background.setPixmap(pixmap)
            self._background.show()

    def _getSongInfo(self) -> SongInfo:
        songInfo: SongInfo = {'artist': '', 'title': '', 'album': '', 'state': ''}
        if Config.MPD:
            if MpdControl.playlistLength() == '0':
                logger.info('Adding all music')
                MpdControl.addAllToPlaylist()

            mpd = MpdControl()
            songInfo = mpd.songInfo()

            if songInfo.get('state', None) is not None and songInfo.get('state', None) != 'play':
                logger.debug(f'Restart playing: {songInfo["state"]}')
                mpd.play()
            mpd.disconnect()
        elif Config.STREAM:
            songInfo['artist'] = self._streamPlayer.streamText
        return songInfo

    def reload(self):
        self._setNewText(self._getSongInfo())
        self._reloadImage()
        self._changeText()
        os.sync()

    @staticmethod
    def reboot():
        logger.info("System rebooting...")
        os.system("sudo reboot")

    def showMenu(self):
        if self.menu is None:
            self.menu = QuickMenu(self._widget, self.cleanupMenu)
        self._widget.menu_down_sig.connect(self.menu.rowDown)
        self._widget.menu_up_sig.connect(self.menu.rowUp)
        self._widget.menu_left_sig.connect(self.menu.changePrev)
        self._widget.menu_right_sig.connect(self.menu.changeNext)
        self.menu.show()

    def cleanupMenu(self):
        logger.debug("cleanup menu")
        self._widget.menu_left_sig.disconnect()
        self._widget.menu_right_sig.disconnect()
        self._widget.menu_down_sig.disconnect()
        self._widget.menu_up_sig.disconnect()

    def showStarMenu(self):
        if self.starMenu is None:
            self.starMenu = StarMenu(self._widget, self.cleanupStarMenu)
#           self.widget.menu_down_sig.connect(self.starMenu.rowDown)
#            self.widget.menu_up_sig.connect(self.starMenu.rowUp)
        self._widget.menu_left_sig.connect(self.starMenu.changePrev)
        self._widget.menu_right_sig.connect(self.starMenu.changeNext)
        self.starMenu.show()
    
    def cleanupStarMenu(self):
        logger.debug('cleanupStarMenu')
        self._widget.menu_left_sig.disconnect()
        self._widget.menu_right_sig.disconnect()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    os.sync()
    myApp = MyApp()
    myApp.reboot_sig.connect(myApp.reboot)
    if Config.MPD:
        myApp.startIdleThread()
    myApp.run()
    
