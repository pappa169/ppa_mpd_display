import os
from subprocess import call
import random
from typing import List, Optional

from config import Config
#from PIL import Image

#from resizeimage import resizeimage

_image_extensions = (".jpg", ".jpeg", ".png", ".JPG", ".JPEG", ".PNG")


class CoverManager:
    def __init__(self, folder=None):
        self.cover_folder = Config.COVER_DIR if folder is None else folder
        random_cover_path = self.cover_folder + "/small"
        if not os.path.exists(random_cover_path):
            os.makedirs(random_cover_path)
        self._random_covers: List[str] = self._get_images_from_folder(random_cover_path)
        self.library_dir = Config.MUSIC_LIBRARY_DIR
        self._hasCover = False

    def hasCover(self):
        return self._hasCover

    def getCover(self, mpd_info) -> Optional[str]:
        self._hasCover = False
        if 'file' in mpd_info:
            song_folder = os.path.dirname(os.path.join(self.library_dir, mpd_info['file']))
            images = self._get_images_from_folder(song_folder)
            if len(images) > 0:
                #  we have a cover in the folder
                if 'artist' in mpd_info and 'album' in mpd_info:
                    # try to load the cover from the folder
                    cover = mpd_info['artist'] + "_" + mpd_info['album'] + ".jpg"
                    cover = self.cover_folder + "/" + cover
                    if os.path.exists(cover):
                        # print ('COVER exists:' + cover)
                        self._hasCover = True
                        return cover
                    else:
                        print('NO COVER')
                        return self._createResizedCover(images[0], cover)
        # return a random cover
        return self._getRandomCover()

    @staticmethod
    def _get_images_from_folder(folder) -> List[str]:
        """Get a list of image files from ``folder``.
        :return list:
            A list of file names from within ``folder`` that end with an extension
            defined in the ``image_extensions`` tuple.
        """
        return [os.path.join(folder, f) for f in os.listdir(folder) if f.endswith(_image_extensions)]

    @staticmethod
    def _createResizedCover(src_cover, dest_cover) -> str:
        print(f'src:{src_cover}, dest: {dest_cover}')
        call(['./resize_cover', src_cover, dest_cover])
        '''with open(src_cover, 'r+b') as f:
            with Image.open(f) as image:
                width, height = image.size
                if width > 656 or height > 416:
                    cover = resizeimage.resize('thumbnail', image, [656, 416])
                    cover.save(dest_cover, image.format)
                else:
                    return src_cover'''
        return dest_cover

    def _getRandomCover(self) -> Optional[str]:
        return self._random_covers[random.randint(0, len(self._random_covers) - 1)] if len(self._random_covers) else None

