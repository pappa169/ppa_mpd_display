import logging
import signal
import subprocess
import threading
from threading import Thread
from subprocess import Popen
from typing import Optional, Callable
import re

logger = logging.getLogger(__name__)


class StreamPlayer:

    def __init__(self, streamUrl: str, callback: Callable):
        self._streamUrl = streamUrl
        self._streamText: Optional[str] = None
        self._pipe: Optional[Popen] = None
        self._thread: Optional[Thread] = None
        self._callback = callback
        self._shutdown: bool = False

    def shutdown(self):
        self._shutdown = True

    @property
    def streamUrl(self) -> str:
        return self._streamUrl

    @streamUrl.setter
    def streamUrl(self, url: str):
        self._streamUrl = url

    @property
    def streamText(self) -> str:
        return self._streamText

    def run(self):
        self._thread = threading.Thread(target=self._start)
        self._thread.start()
        logger.info(f'Thread started with threadId: {self._thread.native_id}')

    def stop(self):
        if self._pipe is not None:
            self._pipe.terminate()
        if self._thread is not None:
            self._thread.join()
        self._pipe = None

    def _start(self):

        def subprocess_setup():
            # Python installs a SIGPIPE handler by default. This is usually not what
            # non-Python subprocesses expect.
            signal.signal(signal.SIGPIPE, signal.SIG_DFL)

        if self._pipe is None:
            pass
            self._pipe = subprocess.Popen(['mplayer', '-slave', self._streamUrl], preexec_fn=subprocess_setup,
                                          stdout=subprocess.PIPE, universal_newlines=True, bufsize=1)
        for line in self._pipe.stdout:
            if line.encode('utf-8').startswith(b'ICY Info:'):
                info = line.split(':', 1)[1].strip()
                attrs = dict(re.findall("(\w+)='([^']*)'", info))
                self._streamText = attrs.get('StreamTitle', '(none)')
                self._callback()

        if self._shutdown is False:
            logger.info('Subprocess stopped from outside, restarting it...')
            self._pipe.terminate()
            self._pipe = None
            self._start()
