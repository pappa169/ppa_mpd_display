from PyQt5 import QtCore,QtWidgets,QtGui
from my_mpd import MpdControl
from typing import List


class StarMenu():
    def __init__(self, parent_widget, cleanupFunc):
        self.__cleanup = cleanupFunc
        self._shown = False
        #timer
        self._menuTimer = QtCore.QTimer()
        self._menuTimer.setInterval(5000)
        self._menuTimer.timeout.connect(self.hide)
        self.table = QtWidgets.QTableWidget(1,2,parent_widget)
        self.table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.table.resize(656, 100)
        self.table.verticalHeader().hide()
        self.table.horizontalHeader().hide()
        self.table.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        font = QtGui.QFont("Sans", 32, QtGui.QFont.Normal)
        self.table.setFont(font)
#        rows=['Song']
        self.table.setRowCount(1)
#        for row in range(self.table.rowCount()):
#            print(row,":", rows[row])
#            self.table.setItem(row, 0, QtWidgets.QTableWidgetItem(rows[row]))
        self._currentTitle = 0
#        self.__currentRow = 0
        self._updateMpd()

    def changeNext(self):
        self._menuTimer.start()
        self._currentTitle += 1
        self._currentTitle = self._currentTitle % len(self._starredSongs)

        self._updateTable()

    def changePrev(self):
        self._menuTimer.start()
        self._currentTitle -= 1
        self._currentTitle = abs(self._currentTitle % len(self._starredSongs))

        self._updateTable()

    def _updateTable(self):
        title = self._starredSongs[self._currentTitle] if len(self._starredSongs) and\
                                                          len(self._starredSongs) > self._currentTitle else ''
        self.table.setItem(0, 0, QtWidgets.QTableWidgetItem(title))
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()
        
    def _updateMpd(self):
        mpd = MpdControl()
        self._starredSongs: List[str] = mpd.getStarredSongs()

    def show(self):
        self._shown = True
        self._updateMpd()
        self._updateTable()
        self.table.show()
        self.table.selectRow(0)
        self._menuTimer.start()

    def hide(self):
        mpd = MpdControl()
#        if self.__currentRow == 1:
        mpd.Playfile(self._starredSongs[self._currentTitle])
        self.table.hide()
        self._menuTimer.stop()
        self.__cleanup()
        self._shown = False

    def isShown(self):
        return self._shown

'''    def rowDown(self):
        self._menuTimer.start()
        if self.__currentRow < self.table.rowCount():
            self.__currentRow += 1
            self.table.selectRow(self.__currentRow)

    def rowUp(self):
        self._menuTimer.start()
        if self.__currentRow > 0:
            self.__currentRow -= 1
            self.table.selectRow(self.__currentRow)'''



