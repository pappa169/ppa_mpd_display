import alsaaudio


def GetVolume() -> int:
    return alsaaudio.Mixer().getvolume()[0]


def SetVolume(volume: int):
    return alsaaudio.Mixer().setvolume(volume)
